import { SettingsState } from "../../settings/store";
import { User } from "../../auth/model";
import { AuthState } from "../../auth/store";
import { GameSetupState } from "../../game/store/game-setup";
import { GamePlayState } from "../../algorithm/store";

export interface State {
  settings: SettingsState.State;
  auth: AuthState.State;
  gameSetup: GameSetupState.State;
  gamePlay: GamePlayState.State;

}
