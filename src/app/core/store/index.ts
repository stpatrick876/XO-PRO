import * as RootStoreSelectors from './root.selectors';
import { State  as RootStoreState} from './state';
import { ActionReducerMap } from '@ngrx/store';
import { AuthReducer } from '../../auth/store';
import { SettingsReducer } from '../../settings/store';
import { ActionReducer } from '@ngrx/store';
export * from '../../settings/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { MetaReducer } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { GameSetupReducer } from '../../game/store/game-setup';
import * as AppState from './state';
import { GamePlayReducer } from '../../algorithm/store';
import { localStorageSync } from 'ngrx-store-localstorage';

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<RootStoreState> = {
  auth: AuthReducer.userReducer,
  settings: SettingsReducer.settingsReducer,
  gameSetup: GameSetupReducer.gameSetupReducer,
  gamePlay: GamePlayReducer.gamePlayReducer

};


export { RootStoreState, RootStoreSelectors };


export function logger(reducer: ActionReducer<RootStoreState>): ActionReducer<RootStoreState> {
  return function(state: RootStoreState, action: any): RootStoreState {
    return reducer(state, action);
  };
}

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({keys: ['auth', 'settings', 'gameSetup', 'gamePlay'], rehydrate: true})(reducer);
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<RootStoreState>[] = !environment.production
  ? [ storeFreeze, localStorageSyncReducer]
  : [];


export {AppState}
