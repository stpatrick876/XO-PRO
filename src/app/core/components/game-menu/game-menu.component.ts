import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameMode } from '../../../settings/setting.model';
import { Store } from '@ngrx/store';
import { SettingsState, SettingsActions } from '../../../settings/store';
import { CommonService } from '../../../shared/services/common/common.service';
import { OnDestroy } from '@angular/core';
import { User, UserType } from '../../../auth/model';
import { AuthSelectors as fromAuth } from '../../../auth/store';
import { Observable } from 'rxjs/internal/Observable';
import { GameSetupActions, GameSetupState } from '../../../game/store/game-setup';
import { AppState } from '../../store';
import { GameSettingsComponent } from '../../../game/components/game-settings/game-settings.component';
import { ViewChild } from '@angular/core';
import { LoaderService } from '../../../shared/services/loader/loader.service';

@Component({
  selector: 'xopro-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.scss']
})
export class GameMenuComponent implements OnInit, OnDestroy {
  GameMode = GameMode;
  user: User;
  UserType = UserType;
  @ViewChild(GameSettingsComponent) gameSettingsMenu: GameSettingsComponent;
  constructor(private store$: Store<AppState.State>, private commonService: CommonService, private loader: LoaderService ) { }

  ngOnInit() {
    this.commonService.setPageTitle('Game Menu');
    this.store$.select(fromAuth.selectAuthUser)
                .subscribe(user => {
                  this.user = user;
                });
  }

  onModeSelection(mode: GameMode) {
  const onSettingsClosed = this.gameSettingsMenu.launch().onClose()

  onSettingsClosed.subscribe((settings: any) => {
    if(!settings.start) {
      return false;
    }
    this.loader.show();
    this.store$.dispatch(new GameSetupActions.SetupGame({
      mode, user: this.user, scoreMode: settings.scoreMode, difficulty: settings.difficulty,
      length: settings.gameLength, board: settings.board, piece: settings.piece, playerPiece: settings.playerPiece, oponentPiece: settings.oponentPiece}));
  });
  }

  ngOnDestroy() {
    this.commonService.clearPageTitle()
  }

}
