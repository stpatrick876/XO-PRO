import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../../shared/services/common/common.service';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'xopro-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit, OnDestroy {

  constructor(private router: Router, private commonService: CommonService ) { }

  ngOnInit() {
    this.commonService.setPageTitle('Page Not Found');
  }

  goHome() {
    this.router.navigate(['/home']);
  }

  ngOnDestroy() {
    this.commonService.clearPageTitle();
  }

}
