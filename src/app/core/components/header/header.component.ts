import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../auth/model';
import { Store } from '@ngrx/store';
import { RootStoreState } from '../../store';
import { userActions, AuthState, AuthSelectors as fromAuth } from '../../../auth/store';
import { ActivatedRoute } from '@angular/router';
import { CommonService } from '../../../shared/services/common/common.service';
import { Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { GamePlayState, GamePlaySelectors as fromGamePlay } from '../../../algorithm/store';
import { GAME_STATUS } from '../../../algorithm/game-play.model';

@Component({
  selector: 'xopro-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
   user: User;
   title: string;
   openMenu: boolean = false;
   @Output() menuOpen = new EventEmitter<boolean>();
   isGameRunning: boolean;
  constructor(private store$: Store<AuthState.State | GamePlayState.State>, private commonService: CommonService, private router: Router) { }

  ngOnInit() {
    this.commonService.getPageTitle().subscribe(title => {
      this.title = title;
    })

    this.store$.select(fromAuth.selectAuthUser).subscribe((user: User) => {
      this.user = user;
    });

    this.store$.select(fromGamePlay.selectGameStatus).subscribe((status: GAME_STATUS) => {
      this.isGameRunning = status === GAME_STATUS.RUNNING;
    });

  }

  toggleMenu() {
    this.openMenu = !this.openMenu;
    this.menuOpen.emit(this.openMenu);
  }

  // TODO: consolidate into single method if posible
  logOut() {
    this.store$.dispatch(new userActions.Logout());
    this.toggleMenu();
  }

  goToSettings() {
    this.router.navigate(['/home/settings']);
    this.toggleMenu();
  }

  exitGame() {
    this.router.navigate(['/home/menu']);
    this.toggleMenu();
  }
}
