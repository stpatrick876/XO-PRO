import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SettingsSelectors, RootStoreState } from "../../store";
import { Store } from "@ngrx/store";
import { LoaderService } from "../../../shared/services/loader/loader.service";
import { Observable } from "rxjs";
import { User } from "../../../auth/model";
import { OverlayContainer } from '@angular/cdk/overlay';

import {
  userActions,
  AuthState,
  AuthSelectors as fromAuth
} from "../../../auth/store";
import { HostBinding } from "@angular/core";
import { SettingsState } from "src/app/settings/store";
import { select } from "@ngrx/store";
import { routeAnimations } from "../../animations";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [routeAnimations]

})
export class AppComponent implements OnInit {
  openMenu: boolean = false;
  @HostBinding("class") componentCssClass;
  settings: any;
  loaderState: any;
  constructor(
    private store$: Store<AuthState.State | SettingsState.State>,
    private loaderService: LoaderService,
    public overlayContainer: OverlayContainer,

  ) {
    loaderService.loaderState.subscribe(state => {
      this.loaderState = state;
    });
  }

  ngOnInit() {
    this.store$.dispatch(new userActions.GetUser());

    this.store$
      .select(fromAuth.selectAuthIsLoading)
      .subscribe((loading: boolean) => {
        if (loading) {
          this.loaderService.show();
        } else {
          this.loaderService.hide();
        }
      });

      this.subscribeToSettings();
  }

  onMenuOpen(e) {
    this.openMenu = e;
  }

  private subscribeToSettings() {
   this.store$
     .pipe(select(SettingsSelectors.selectSettingsState))
     .subscribe(settings => {
       this.settings = settings;
       console.log(settings)
       this.setTheme(settings);
     });
 }

  private setTheme(settings: any) {
  const hours = new Date().getHours();
  console.log(settings)
  const effectiveTheme = settings.theme;
  this.componentCssClass = effectiveTheme;
  }
}
