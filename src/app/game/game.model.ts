import { User } from "../auth/model";



export interface Stats {
      player: number;
      oponent: number;
}

export enum ScoreMode {
  SINGLEROUND = 'Sinlge Round',
  BESTOFTHREE = 'Best of Three',
  TIMED = 'Timed Game',
}

export enum DificultyLevel {
  EASY,
  NOVICE,
  MASTER

}

export enum PLAYER_TYPE {
  PLAYER = 'Player',
  OPONENT = 'Oponent'
}

interface GameSettings {
  start: boolean;
  scoreMode: ScoreMode;
}
