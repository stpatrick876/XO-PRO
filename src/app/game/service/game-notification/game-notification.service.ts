import { Injectable } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
export enum MessageType {
   WIN,
   LOSS,
   TURN,
   TIMEALERT
}


@Injectable()
export class GameNotificationService {
  private message$ = new ReplaySubject();


   constructor() { }
   showMessage(text: string) {
  this.message$.next(text);
   }

   clearMessage() {

   }

   nextMessage(type: MessageType, obj: any) {
       let res = '';
       switch(type){
         case MessageType.TURN:
         res = `Ready Player ${obj.displayName}`
       }

      this.message$.next(res)
   }

   get message() {
     return this.message$.asObservable();
   }


}
