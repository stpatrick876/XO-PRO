import { TestBed, inject } from '@angular/core/testing';

import { GameNotificationService } from './game-notification.service';

describe('GameNotificationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GameNotificationService]
    });
  });

  it('should be created', inject([GameNotificationService], (service: GameNotificationService) => {
    expect(service).toBeTruthy();
  }));
});
