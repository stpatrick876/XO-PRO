import * as gameSetupActions from './game-setup.actions';
import { State, initialState } from "./state";

export type Action = gameSetupActions.All;
/// Reducer function
export function gameSetupReducer(state: State = initialState, action: Action) {
  switch (action.type) {

    case gameSetupActions.SETUP_GAME:
        return { ...state, ...action.payload, isLoading: true };
    case gameSetupActions.GAME_READY:
        return { ...state, ...action.payload, isLoading: false };
        default:
            return state;

  }
}
