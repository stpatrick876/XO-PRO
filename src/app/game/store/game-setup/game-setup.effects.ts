import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of as observableOf, of, forkJoin } from 'rxjs';
import 'rxjs/add/operator/merge';

import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';

import * as gameSetupActions from './game-setup.actions';
import { Router } from '@angular/router';
import { GameMode } from '../../../settings/setting.model';
import { Machine } from '../../../auth/model';
export type Action = gameSetupActions.All;


@Injectable()
export class GameEffects {
  constructor(private actions$: Actions, private router: Router) {}


    @Effect()
       setupGame:  Observable<Action> = this.actions$.ofType(gameSetupActions.SETUP_GAME)
                                            .map((action: gameSetupActions.SetupGame) => action.payload )
                                            .delay(2000) // delay to show loading spinner, delete me!
                                            .switchMap(payload =>{
                                                return this.settup(payload).switchMap((gameInfo) => {
                                                  this.router.navigate(['/home/game'])
                                                  return  of(new gameSetupActions.GameReady(gameInfo));
                                                })
                                            })

      private settup(config: any): Observable<any> {
            var initialGameInfo = {
              players: {
                player: config.user,
                oponent: Machine
              }
            }

        return of(initialGameInfo);
      }



}
