import { Stats, ScoreMode, DificultyLevel } from "../../game.model";
import { User } from "../../../auth/model";
import { DynomicBoardItem } from "../../../board/board.model";

export interface State {
    board: DynomicBoardItem | null;
    piece: any;
    players: {
      player: User | null;
      oponent: User | null
    };
    scoreMode: ScoreMode;
    difficulty?: DificultyLevel;
    stats: Stats | null;
    length: number;
    isLoading: boolean;
    error: string;
    playerPiece: any,
    oponentPiece: any
}


export const initialState: State = {
  board: null,
  piece: null,
  length: 0,
  players: {
    player: null,
    oponent: null
  },
  playerPiece: null,
  oponentPiece: null,
  scoreMode: ScoreMode.SINGLEROUND,
  stats: {
    player: 0,
    oponent: 0
  },
  isLoading: false,
  error: null
}
