import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector
} from '@ngrx/store';

import { State } from './state';
import { ScoreMode, DificultyLevel } from '../../game.model';
import { BOARD_NAME } from '../../../board/board.model';

const getError = (state: State): any => state.error;

const getIsLoading = (state: State): boolean => state.isLoading;

const getPlayers = (state: State): any => state.players;

const getScoreMode = (state: State): any => state.scoreMode;

const getAiDificulty = (state: State): any => state.difficulty;

const getBoard = (state: State): any => state.board;

const getPiece = (state: State): any => state.piece;

const getGameLength = (state: State): any => state.length;

const getPlayerPiece = (state: State): any => state.playerPiece;

const getOpenentPiece = (state: State): any => state.oponentPiece;

export const selectGameState: MemoizedSelector<object, State> = createFeatureSelector<State>('gameSetup');

export const selectGamePlayers: MemoizedSelector<object, any> = createSelector(selectGameState, getPlayers);

export const selectScoreMode: MemoizedSelector<object, ScoreMode> = createSelector(selectGameState, getScoreMode);

export const selectBoard: MemoizedSelector<object, BOARD_NAME> = createSelector(selectGameState, getBoard);

export const selectGameLength: MemoizedSelector<object, number> = createSelector(selectGameState, getGameLength);

export const selectPieceForSelectedBoard: MemoizedSelector<object, any> = createSelector(selectGameState, getPiece);

export const selectPlayerPiece: MemoizedSelector<object, any> = createSelector(selectGameState, getPlayerPiece);

export const selectOpenentPiece: MemoizedSelector<object, any> = createSelector(selectGameState, getOpenentPiece);

export const selecAiDifficultyLevel: MemoizedSelector<object, DificultyLevel> = createSelector(selectGameState, getAiDificulty);
