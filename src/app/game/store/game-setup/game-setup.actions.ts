import { Action } from "@ngrx/store";

export const SETUP_GAME              = '[GAME SETUP] Setup Game';
export const GAME_READY              = '[GAME SETUP] Game ready';



export class SetupGame implements Action {
    readonly type = SETUP_GAME;
    constructor(public payload?: any) {}
}

export class GameReady implements Action {
    readonly type = GAME_READY;
    constructor(public payload?: any) {}
}

export type All = SetupGame | GameReady ;
