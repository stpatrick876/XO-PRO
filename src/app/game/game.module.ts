import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './components/game/game.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GameEffects } from './store/game-setup/game-setup.effects';
import { gameSetupReducer } from './store/game-setup/game-setup.reducer';
import { GameSettingsComponent } from './components/game-settings/game-settings.component';
import { SharedModule } from '../shared/shared.module';
import {MatSliderModule} from '@angular/material/slider';
import { FormsModule }    from '@angular/forms';
import { GameTimerComponent } from './components/game-timer/game-timer.component';
import { BoardModule } from '../board/board.module';
import { BoardOneComponent } from '../board/components/boards/board-one/board-one.component';
import { BlackWhiteBoardComponent } from '../board/components/boards/black-white-board/black-white-board.component';
import { BlackSilverBoardComponent } from '../board/components/boards/black-silver-board/black-silver-board.component';
import { NgDragDropModule } from 'ng-drag-drop';
import { PlayerTrayComponent } from './components/player-tray/player-tray.component';
import { GameNotificationComponent } from './components/game-notification/game-notification.component';
import { TeximateModule } from 'ng-teximate';
import { GameNotificationService } from './service/game-notification/game-notification.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgDragDropModule.forRoot(),
    StoreModule.forFeature('gameSetup', gameSetupReducer),
    EffectsModule.forFeature([GameEffects]),
    MatSliderModule,
    FormsModule,
    BoardModule,
    TeximateModule

  ],
  declarations: [
    GameComponent,
    GameSettingsComponent,
    GameTimerComponent,
    PlayerTrayComponent,
    GameNotificationComponent
  ],
  exports: [GameComponent, GameSettingsComponent],
  providers: [GameEffects, GameNotificationService]

})
export class GameModule { }
