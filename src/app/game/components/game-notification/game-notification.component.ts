import { Component, OnInit } from '@angular/core';
import { TeximateOptions, TeximateOrder, TeximateComponent } from 'ng-teximate';
import { ViewChild } from '@angular/core';
import { GameNotificationService } from '../../service/game-notification/game-notification.service';

@Component({
  selector: 'xopro-game-notification',
  templateUrl: './game-notification.component.html',
  styleUrls: ['./game-notification.component.scss']
})
export class GameNotificationComponent implements OnInit {
  @ViewChild(TeximateComponent) teximate: TeximateComponent;

  text : string = "";

  constructor(private notif: GameNotificationService) { }

  ngOnInit() {
    const diffOptions: TeximateOptions = {
      type: 'letter',
      animation: { name: 'jello', duration: 1000 },
      word: { type: TeximateOrder.SEQUENCE, delay: 100 },
      letter: { type: TeximateOrder.SEQUENCE, delay: 50 }
    };

    this.notif.message.subscribe((message: string) => {
      this.text = message;
      this.teximate.runEffect(diffOptions);

    })



  }



}
