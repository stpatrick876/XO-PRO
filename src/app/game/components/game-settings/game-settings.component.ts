import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../shared/services/common/common.service';
import { Observable } from 'rxjs/internal/Observable';
import { of, Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { ScoreMode, DificultyLevel } from '../../game.model';
import { BoardOption, boardOptions } from '../../../board/board.model';
import { PIECE_NAME, PieceType } from '../../../board/piece.model';




@Component({
  selector: 'xopro-game-settings',
  templateUrl: './game-settings.component.html',
  styleUrls: ['./game-settings.component.scss']
})
export class GameSettingsComponent implements OnInit {
    scroreModes: any[] = [];
    pieceTypes: any[] = [];
    difficultyLevels: any[] = [];
    settings: Subject<any> = new Subject();
    selectedModeIndex: number =  0;
    selectedBoardIndex: number =  0;
    selectedPieceTypendex: number =  0;
    selectedDifficultyIndex: number =  0;
    time = 5;
    boardOptions: BoardOption[] = boardOptions;
  constructor(private commonService: CommonService) { }

  ngOnInit() {
   this.scroreModes = this.commonService.enumToArray(ScoreMode);

   this.pieceTypes = this.commonService.enumToArray(PieceType, true);
   this.difficultyLevels = this.commonService.enumToArray(DificultyLevel, true);
  }

displayWith = (value: number | null) => value ? `${value}m` : '';

     isSelectedMode(i) {
      return this.selectedModeIndex === i;
    }
    isSelectedBoard(i) {
     return this.selectedBoardIndex === i;
   }
   isSelectedPieceType(i) {
    return this.selectedPieceTypendex === i;
  }
  isSelectedDifficulty(i) {
   return this.selectedDifficultyIndex === i;
 }

  close(shouldStart: boolean) {
    const mode = ScoreMode[this.scroreModes[this.selectedModeIndex].name];
    const difficulty = DificultyLevel[this.difficultyLevels[this.selectedDifficultyIndex].name];
    const board = this.boardOptions[this.selectedBoardIndex].name;
    const piece = this.commonService.enumToArray(PIECE_NAME)[this.selectedBoardIndex];
    const playerPiece = this.pieceTypes[this.selectedPieceTypendex];
    const oponentPiece = CommonService._.difference(this.pieceTypes, [playerPiece]);

    this.commonService.JQuery('#gameMenuModal').modal('hide');
      this.settings.next({
        start: shouldStart,
        scoreMode: mode,
        difficulty: difficulty,
        gameLength: this.time,
        board:  board,
        piece: piece,
        playerPiece: playerPiece,
        oponentPiece: oponentPiece[0]
      });
  }



  launch() {
    var $this = this;
    this.commonService.JQuery('#gameMenuModal').modal('show');
    return {
      onClose: () => $this.settings.asObservable().take(1)
    }
  }

}
