import { Component, OnInit } from "@angular/core";
import { AuthState, AuthSelectors as fromAuth } from "../../../auth/store";
import { Store } from "@ngrx/store";
import { User, UserType } from "../../../auth/model";
import { LoaderService } from "../../../shared/services/loader/loader.service";
import {
  GameSetupState,
  GameSetupSelectors as fromGameSetup
} from "../../store/game-setup";
import { ViewChild } from "@angular/core";
import { BoardHostDirective } from "../../../board/directives/board-host/board-host.directive";
import { BoardService } from "../../../board/services/board/board.service";
import {
  BOARD_NAME,
  IBoardComponent,
  DynomicBoardItem
} from "../../../board/board.model";
import { ComponentFactoryResolver } from "@angular/core";
import {
  GamePlayState,
  GamePlayActions,
  GamePlaySelectors as fromGamePlay
} from "../../../algorithm/store";
import { of, Observable } from "rxjs";
import { ComponentRef } from "@angular/core";
import { AppState } from "../../../core/store";
import { PLAYER_TYPE, DificultyLevel } from "../../game.model";
import { AiService } from "../../../algorithm/services/ai/ai.service";
import { ShadowBoardCell } from "../../../algorithm/game-play.model";
import { BoardComponentBase } from "../../../board/BoardComponentBase";
import { DynamicPieceItem } from "../../../board/piece.model";
import { PieceService } from "../../../board/services/piece/piece.service";

@Component({
  selector: "xopro-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"]
})
export class GameComponent implements OnInit {
  user: User;
  @ViewChild(BoardHostDirective) boardHost: BoardHostDirective;
  board: any;

  PLAYER_TYPE = PLAYER_TYPE;
  players: any;

  constructor(
    private store$: Store<GameSetupState.State | GamePlayState.State>,
    private loader: LoaderService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private boardService: BoardService,
    private aiService: AiService,
    private pieceService: PieceService
  ) {}

  ngOnInit() {
    this.loader.show();

    this.store$
      .combineLatest(
        this.store$.select(fromGameSetup.selectBoard),
        this.store$.select(fromGameSetup.selectGamePlayers)
      )
      .take(1)
      .subscribe(stateSelection => {
        const boardName = stateSelection[1];
        this.players = stateSelection[2];

        this.boardService.getBoard(boardName).subscribe((board: any) => {
          this.loadBoardComponent(board).subscribe(componetRef => {
            if (!componetRef) {
              return false;
            }
            this.board = componetRef;
            this.store$.dispatch(new GamePlayActions.StartGame());
            this.loader.hide();
          });
        });
      });

    this.handleAdInEasyMode();
  }

  handleAdInEasyMode() {
    this.store$.select(fromGamePlay.selectTurn).subscribe((turn: any) => {
      if (
        this.players.oponent.type === UserType.MACHINE &&
        turn === PLAYER_TYPE.OPONENT
      ) {
        this.store$
          .select(fromGameSetup.selectOpenentPiece)
          .delay(3000)
          .subscribe(oponentPiece => {
            this.store$
              .select(fromGameSetup.selecAiDifficultyLevel)
              .subscribe(level => {
                this.aiService
                  .notify(
                    DificultyLevel[level],
                    this.players.oponent,
                    oponentPiece
                  )
                  .subscribe((cell: ShadowBoardCell) => {
                    this.store$
                      .select(fromGameSetup.selectPieceForSelectedBoard)
                      .map(res => res.label)
                      .subscribe(pieceName => {
                        this.pieceService
                          .getPiece(pieceName)
                          .subscribe((piece: DynamicPieceItem) => {
                            this.board.instance
                              .loadPieceComponentInCell(
                                cell.id - 1,
                                piece,
                                Number(oponentPiece.name),
                                this.players.oponent
                              )
                              .subscribe(() => {
                                this.store$.dispatch(
                                  new GamePlayActions.CheckTerminal()
                                );
                              });
                          });
                      });
                  });
              });
          });
      }
    });
  }

  loadBoardComponent(
    board: DynomicBoardItem
  ): Observable<boolean | ComponentRef<IBoardComponent>> {
    if (!board) {
      return of(false);
    }
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      board.component
    );
    let viewContainerRef = this.boardHost.viewContainerRef;
    viewContainerRef.clear();

    let componentRef = viewContainerRef.createComponent(componentFactory);
    (<IBoardComponent>componentRef.instance).data = board.data;

    return of(componentRef);
  }
}
