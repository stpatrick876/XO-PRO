import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerTrayComponent } from './player-tray.component';

describe('PlayerTrayComponent', () => {
  let component: PlayerTrayComponent;
  let fixture: ComponentFixture<PlayerTrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerTrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerTrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
