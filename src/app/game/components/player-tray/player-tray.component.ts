import { Component, OnInit } from '@angular/core';
import { DynamicPieceItem, IDynamicPieceItem, PieceType } from '../../../board/piece.model';
import { Store } from '@ngrx/store';
import { GameSetupState, GameSetupSelectors as  fromGameSetup } from '../../store/game-setup';
import { GamePlayState, GamePlaySelectors as fromAlgo } from '../../../algorithm/store';
import { ComponentFactoryResolver } from '@angular/core';
import { PieceService } from '../../../board/services/piece/piece.service';
import { Input } from '@angular/core';
import { ViewChild } from '@angular/core';
import { PieceHostDirective } from '../../../board/directives/piece-host/piece-host.directive';
import { ComponentRef } from '@angular/core';
import { PLAYER_TYPE } from '../../game.model';
import { User, UserType } from '../../../auth/model';
import { AfterViewInit } from '@angular/core';
import { GameNotificationService, MessageType } from '../../service/game-notification/game-notification.service';
import { AiService } from '../../../algorithm/services/ai/ai.service';

@Component({
  selector: 'xopro-player-tray',
  templateUrl: './player-tray.component.html',
  styleUrls: ['./player-tray.component.scss']
})
export class PlayerTrayComponent implements OnInit, AfterViewInit {
  pieceType = PieceType;
  open = false;
  @Input() player: User;
  @Input() trayType: PLAYER_TYPE;
  @ViewChild(PieceHostDirective) pieceHost: PieceHostDirective;
  componentRef: ComponentRef<IDynamicPieceItem>;
  turn: PLAYER_TYPE;
  constructor(private pieceService: PieceService, private componentFactoryResolver: ComponentFactoryResolver,
     protected store$: Store<GameSetupState.State | GamePlayState.State>,
     private notif: GameNotificationService) { }

  ngOnInit() {}

  ngAfterViewInit(): void {
    let pieceTypeSelector;

    if (this.trayType === PLAYER_TYPE.PLAYER) {
        pieceTypeSelector = this.store$.select(fromGameSetup.selectPlayerPiece);
    } else {
      pieceTypeSelector = this.store$.select(fromGameSetup.selectOpenentPiece);
    }

      this.store$.combineLatest(this.store$.select(fromGameSetup.selectPieceForSelectedBoard), pieceTypeSelector,
      this.store$.select(fromGameSetup.selectGamePlayers)).subscribe(stateRes => {
          const pieceData = stateRes[1];
          const pieceTpe = stateRes[2];
          const players = stateRes[3];

          const player = this.trayType === PLAYER_TYPE.PLAYER? players.player : players.oponent;

        if(!pieceData || !pieceTpe) {
          return false;
        }
        this.pieceService.getPiece(pieceData.label).subscribe((piece: DynamicPieceItem) => {
          this.loadPieceComponent(piece, pieceTpe, player);
        });
      });


   this.store$.combineLatest(this.store$.select(fromAlgo.selectTurn), this.store$.select(fromGameSetup.selectGamePlayers))
   .subscribe((stateSelection: any) => {
     const turn = stateSelection[1];
     const players = stateSelection[2];

      const nextPlayer = turn === PLAYER_TYPE.OPONENT ? players.oponent : players.player;

     this.notif.nextMessage(MessageType.TURN, nextPlayer);


       this.turn = turn;
           if(!this.componentRef) {
             return false;
           }
         (<IDynamicPieceItem>this.componentRef.instance).data.activeTurn = this.trayType === turn;


   })
  }



  loadPieceComponent(piece: DynamicPieceItem, pieceTpe, player) {
    if(!piece || !this.pieceHost) {
      return false;
    }
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(piece.component);
    let viewContainerRef = this.pieceHost.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
    const data = piece.data;
    data.type = PieceType[pieceTpe.label];
    data.player = player;
  (<IDynamicPieceItem>this.componentRef.instance).data = data;
  }

}
