import { Component, OnInit } from '@angular/core';
import { Subject, interval, of } from 'rxjs';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/combineLatest';

import { Store } from '@ngrx/store';
import { GameSetupState, GameSetupSelectors as fromGameSetup } from '../../store/game-setup';
import { ScoreMode } from '../../game.model';


 interface TimerMode {
    text: string;
    direction: 'up' | 'down';
 }
@Component({
  selector: 'xopro-game-timer',
  templateUrl: './game-timer.component.html',
  styleUrls: ['./game-timer.component.scss']
})
export class GameTimerComponent implements OnInit {
  timerMode: TimerMode = {
    text: '',
    direction: 'down'
  };
   open = false;
   time = {
     min: '10',
     sec: '20'
   }
   inAlert = false;
   initialValue  = 0;
   subject = new Subject()
   startTrigger$ =  this.subject.asObservable();

   constructor(private store$: Store<GameSetupState.State>) { }

   ngOnInit() {
     this.store$.combineLatest(this.store$.select(fromGameSetup.selectScoreMode), this.store$.select(fromGameSetup.selectGameLength))
     .subscribe((state) => {
       const gameLength = state["2"];
       const gameMode  = state["1"];
       if (gameMode == ScoreMode.TIMED) {
           this.timerMode.text = 'Time Remaining'
           this.timerMode.direction = 'down'

       } else {
         this.timerMode.text = 'Time Elapsed'
         this.timerMode.direction = 'up'
       }
        this.initTimer(gameLength);
        this.subject.next(true);
     });
   }

     initTimer(gameLength: number) {
         // Options
         const $this = this;
     const totalTime = gameLength * 60;
     const alertTime = totalTime * 0.30 ;
     // Helper stuff
     const toTime = (seconds) => ({
       seconds: Math.floor(seconds % 60),
       minutes: Math.floor(seconds / 60),
       alert: this.timerMode.direction  === 'up' ? false : seconds < alertTime
     });
     const pad = (number) => number <= 9 ? ('0' + number) : number.toString();

     const initialValue = this.timerMode.direction === 'up' ? 0 : totalTime;


     const dec = (acc) => acc - 1;
     const inc = (acc) => acc + 1;

     const incOrDec = (acc) => (this.timerMode.direction === 'up' ? inc : dec)(acc)

     const timerNotEnded = (seconds) => this.timerMode.direction  === 'up' ? true : seconds >= 0;

     const render = (time) => {
      $this.time.min = pad(time.minutes);
      $this.time.sec = pad(time.seconds);

      $this.inAlert = time.alert;
     }

     const interval$ = interval(1000);


     const toggle$ =  $this.startTrigger$.mapTo((isRunning) => !isRunning)
                                   .startWith((false as any))
                                   .scan((isRunning, toggleOrFalse) =>  toggleOrFalse(isRunning))
                                   .share();

     const start$ = toggle$.filter((isRunning) => isRunning);
     const stop$ = toggle$.filter((isRunning) => !isRunning);
     const incrementOrDecrement$ = interval$.takeUntil(stop$).mapTo(incOrDec);

     start$
     .do(() => this.open = true)
       .switchMapTo(incrementOrDecrement$)
       .startWith((initialValue as any))
       .scan((seconds, incrementOrDecrement) => incrementOrDecrement(seconds))
       .takeWhile(timerNotEnded)
       .map(toTime)
       .subscribe(render);

     }
}
