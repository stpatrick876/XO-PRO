import { Component, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import {  LoaderService,  } from '../../../services/loader/loader.service';
import { Subscription } from 'rxjs';
import { LoaderState } from '../loader';

@Component({
  selector: 'xopro-sphere-loader',
  templateUrl: './sphere-loader.component.html',
  styleUrls: ['./sphere-loader.component.scss']
})
export class SphereLoaderComponent implements OnInit, OnDestroy {
  show = false;
  private subscription: Subscription;
  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
    this.subscription = this.loaderService.loaderState
      .subscribe((state: LoaderState) => {
          this.show = state.show;
      });
      }

  ngOnDestroy() {
          this.subscription.unsubscribe();
      }
}
