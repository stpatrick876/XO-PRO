import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SphereLoaderComponent } from './sphere-loader.component';

describe('SphereLoaderComponent', () => {
  let component: SphereLoaderComponent;
  let fixture: ComponentFixture<SphereLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SphereLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SphereLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
