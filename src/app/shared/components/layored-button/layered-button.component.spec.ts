import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoredButtonComponent } from './layored-button.component';

describe('LayoredButtonComponent', () => {
  let component: LayoredButtonComponent;
  let fixture: ComponentFixture<LayoredButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoredButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoredButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
