import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'xopro-layered-button',
  templateUrl: './layered-button.component.html',
  styleUrls: ['./layered-button.component.scss']
})
export class LayeredButtonComponent implements OnInit {
  @Input() label: string;
  @Input() path: string;
  constructor() { }

  ngOnInit() {
  }

}
