import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'xopro-label-button',
  templateUrl: './label-button.component.html',
  styleUrls: ['./label-button.component.scss']
})
export class LabelButtonComponent implements OnInit {
  @Input() label: string;
  @Input() type: 'Primary' | 'Secondary' = 'Primary';

  constructor() { }

  ngOnInit() {
  }

}
