import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';


@Component({
  selector: 'xopro-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss']
})
export class IconButtonComponent implements OnInit {
  @Input() icon: string;
  @Input() type: 'primary' | 'secondary' = 'primary';
  constructor() { }

  ngOnInit() {
  }

}
