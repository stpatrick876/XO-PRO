import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { LayeredButtonComponent } from "./components/layored-button/layered-button.component";
import { RouterModule } from "@angular/router";
import { LoaderService } from "./services/loader/loader.service";
import { SphereLoaderComponent } from "./components/loaders/sphere-loader/sphere-loader.component";
import { LocalStorageService } from "./services/localstorage/localstorage.service";
import { UniqueGeneratorService } from "./services/uniqueGenerator/unique-genrator.service";
import { IconButtonComponent } from "./components/icon-button/icon-button.component";
import { CommonService } from "./services/common/common.service";
import { LabelButtonComponent } from "./components/label-button/label-button.component";
import { RadioButtonComponent } from "./components/radio-button/radio-button.component";
import { NgDragDropModule } from 'ng-drag-drop';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from "@angular/forms";
import { DropdownListComponent } from "./components/dropdown-list/dropdown-list.component";

const materialModules = [MatSelectModule];
@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      NgDragDropModule.forRoot(),
      FormsModule,
      ...materialModules
   ],
  declarations: [
    LayeredButtonComponent,
    LabelButtonComponent,
    SphereLoaderComponent,
    IconButtonComponent,
    RadioButtonComponent,
    DropdownListComponent
  ],
  providers: [LoaderService, LocalStorageService, UniqueGeneratorService, CommonService],
  exports: [
    LayeredButtonComponent,
    LabelButtonComponent,
    SphereLoaderComponent,
    IconButtonComponent,
    RadioButtonComponent,
    FormsModule,
    DropdownListComponent,
    ...materialModules
  ]
})

export class SharedModule {}
