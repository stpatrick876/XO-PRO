import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { BehaviorSubject, of } from 'rxjs';

export interface ILocalStorage {
  select(key: string, defaultValue: any ): Observable<any>;
  set(key: string, value: any): void;
  remove(key: string): void;
}

@Injectable()
export class LocalStorageService implements ILocalStorage {
  protected subjects: {[key: string]: BehaviorSubject<any>} = {};

  select(key: string, defaultValue: any = null) {

    if (!window.localStorage.getItem(key) && defaultValue) {
      window.localStorage.setItem(key, JSON.stringify(defaultValue));
    }

    const value = window.localStorage.getItem(key) ? JSON.parse(window.localStorage.getItem(key))  : defaultValue;

    return value;
  }

  set(key: string, value: any): void {

    window.localStorage.setItem(key, JSON.stringify(value));

    if (this.subjects.hasOwnProperty(key)) {
      this.subjects[key].next(value);
    }
  }

  remove(key: string): void {

    window.localStorage.removeItem(key);

    if (this.subjects.hasOwnProperty(key)) {
      this.subjects[key].next(null);
    }
  }
}
