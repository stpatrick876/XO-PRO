import { Injectable } from '@angular/core';
import * as  Chance from 'chance';

@Injectable()
export class UniqueGeneratorService {
  chance: any;
  chosenColors: any[] = ['#d61ff5']; //d61ff5 used for ai
  constructor() {
    this.chance = new Chance();
  }

get guid() {
  return this.chance.guid();
}

get guestName() {
  return `guest_${this.chance.string({ length: 5 })}`;
}

get getUserColor() {
  const color = this.chance.color({format: 'hex'})

  if(this.chosenColors.includes(color)) {
    this.getUserColor();
  } else {
    this.chosenColors.push(color);
    return color;
  }

}
get randomString() {
  return   this.chance.word();
}

}
