import { TestBed, inject } from '@angular/core/testing';

import { UniqueGenratorService } from './unique-genrator.service';

describe('UniqueGenratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UniqueGenratorService]
    });
  });

  it('should be created', inject([UniqueGenratorService], (service: UniqueGenratorService) => {
    expect(service).toBeTruthy();
  }));
});
