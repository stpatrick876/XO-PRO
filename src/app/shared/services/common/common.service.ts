import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, ReplaySubject } from 'rxjs';
import { Observable } from 'rxjs/internal/Observable';
declare const $: any;
declare const _: any;

@Injectable()
export class CommonService {
  private pageTitle = new Subject<any>();

    setPageTitle(title: string) {
        this.pageTitle.next(title);
      //  this.pageTitle.complete()
    }

    clearPageTitle() {
        this.pageTitle.next('');
      //  this.pageTitle.complete()

    }

    getPageTitle(): Observable<any> {
        return this.pageTitle.asObservable();
    }


    // provide jquery


    get JQuery() {
      return $;
    }

    static get _() {
      return _;
    }

    // Helper

// Turn enum into array
 enumToArray(enumme, shouldFilter = false) {
 const StringIsNumber = value => shouldFilter ? isNaN(Number(value)) === false : true;

 const re = Object.keys(enumme)
     .filter(StringIsNumber)
     .map(key => {
      return {
        name: key,
        label: enumme[key]
      }
     });
    return re
}

}
