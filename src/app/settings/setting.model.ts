export enum SettingsType {
  BOARD,
  PIECES,
  THEME
}

export enum GameMode {
  MVM,
  MVML,
  MVMR
}

export interface SettingOption {
type: SettingsType;
index: number;
name: string;
}


export interface BoardSettingOption extends SettingOption {
image: string;
}

export interface PiecesSettingOption extends SettingOption {
  image: string;

}

  export interface ThemeSettingOption extends SettingOption {
    image: string;
  }

export interface GameSetting {
  Board?: BoardSettingOption;
  Pieces?: PiecesSettingOption;
  Theme?: ThemeSettingOption;

}


export enum Theme {
  DARK = 'dark-theme',
  LIGHT = 'light-theme',
  DEFAULT = 'default-theme',
  NATURE = 'nature-theme'
}
