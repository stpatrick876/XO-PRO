import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { of } from 'rxjs';
import { BoardSettingOption, SettingsType } from '../../setting.model';

@Injectable({
  providedIn: 'root'
})
export class GameSettingsService {

  constructor() { }

  getBoardOptions(): Observable<BoardSettingOption[]> {
     const  options: BoardSettingOption[] = [{
       type: SettingsType.BOARD,
       index: 1,
       name: 'plain',
       image: 'https://dummyimage.com/40/40/000/fff'
     }, {
      type: SettingsType.BOARD,
      index: 2,
      name: 'blue ice',
      image: 'https://dummyimage.com/40/40/000/fff'
    }, {
      type: SettingsType.BOARD,
      index: 3,
      name: 'blue ice 2',
      image: 'https://dummyimage.com/40/40/000/fff'
    }];
    return of(options);
  }
}
