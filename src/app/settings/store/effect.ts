import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of as observableOf, of } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import * as settingsActions from './actions';
import { GameSettingsService } from '../services/game-setting/game-settings.service';
import { BoardSettingOption } from '../setting.model';

@Injectable()
export class SettingsStoreEffects {
  constructor(private actions$: Actions, private gameSettingsService: GameSettingsService) {}




}
