import { GameSetting, Theme } from "../setting.model";

export interface State {
  theme: Theme
}

export const initialState: State = {
  theme: Theme.DARK,
};
