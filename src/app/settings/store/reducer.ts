import { Action } from '@ngrx/store';
import { initialState } from './state';
import { CHANGE_THEME, SettingsActions } from './actions';

export function settingsReducer(state = initialState, action: SettingsActions) {
  switch (action.type) {
    case CHANGE_THEME:
    return { ...state, ...action.payload };
    default:
      return state;
  }
}
