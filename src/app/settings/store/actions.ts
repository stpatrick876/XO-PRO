import { Action } from "@ngrx/store";
import { GameSetting, GameMode, BoardSettingOption } from "../setting.model";

export const  CHANGE_THEME = '[Settings] Change Theme';


export class ActionSettingsChangeTheme implements Action {
  readonly type =  CHANGE_THEME;

  constructor(readonly payload: any) {}
}


export type SettingsActions = ActionSettingsChangeTheme ;
