import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';
import { State } from './state';
import { Theme } from '../setting.model';

export const getTheme = (state: State): any => state.theme;

export const selectSettingsState: MemoizedSelector<object, State> = createFeatureSelector<State>('settings');

export const selectTheme: MemoizedSelector<object, Theme> = createSelector(selectSettingsState, getTheme);
