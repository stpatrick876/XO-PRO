import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { GameSettingsService } from './services/game-setting/game-settings.service';
import { settingsReducer } from './store/reducer';
import { SettingsStoreEffects } from './store/effect';
import { SharedModule } from '../shared/shared.module';
import { AppSettingsComponent } from './components/app-settings/app-settings.component';

@NgModule({
  declarations: [AppSettingsComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature('settings', settingsReducer),
    EffectsModule.forFeature([SettingsStoreEffects])
  ],
  providers: [GameSettingsService, SettingsStoreEffects]
})
export class SettingsModule {}
