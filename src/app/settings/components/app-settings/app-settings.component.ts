import { Component, OnInit } from '@angular/core';
import { GameSettingsService } from '../../services/game-setting/game-settings.service';
import { Observable } from 'rxjs';
import { BoardSettingOption, GameSetting, Theme } from '../../setting.model';
import { Store } from '@ngrx/store';
import { SettingsState, SettingsSelectors as fromSettings, SettingsActions } from '../../store';
import { LoaderService } from '../../../shared/services/loader/loader.service';
import { CommonService } from '../../../shared/services/common/common.service';
import { ActionSettingsChangeTheme } from '../../store/actions';
import { select } from '@ngrx/store';
import { takeUntil } from 'rxjs/operators';
import {Location} from '@angular/common';

@Component({
  selector: 'xopro-app-settings',
  templateUrl: './app-settings.component.html',
  styleUrls: ['./app-settings.component.scss']
})
export class AppSettingsComponent implements OnInit {

  themes: any[];
  settings: any;
  constructor( private store$: Store<SettingsState.State>, private commonService: CommonService, private _location: Location ) {
    store$
   .pipe(select(fromSettings.selectSettingsState))
   .subscribe(settings => (this.settings = settings));
  }



   ngOnInit() {
     this.commonService.setPageTitle('Settings');
     this.themes = this.commonService.enumToArray(Theme).map(theme => { return {label: theme.name,value: theme.label}});

     console.log(this.themes)
   }

   onThemeSelect({ value: theme }) {
     this.store$.dispatch(new ActionSettingsChangeTheme({ theme: theme }));
     //this.store.dispatch(new ActionSettingsPersist({ settings: this.settings }));
   }

  onExitSettings() {
    this._location.back();
  }




}
