import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackSilverPiecesComponent } from './black-silver-pieces.component';

describe('BlackSilverPiecesComponent', () => {
  let component: BlackSilverPiecesComponent;
  let fixture: ComponentFixture<BlackSilverPiecesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackSilverPiecesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackSilverPiecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
