import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { PieceBase, PIECE_NAME, Piece } from '../../../piece.model';

@Component({
  selector: 'xopro-black-silver-pieces',
  templateUrl: './black-silver-pieces.component.html',
  styleUrls: ['./black-silver-pieces.component.scss']
})
export class BlackSilverPiecesComponent extends PieceBase  implements OnInit  {
  constructor() {
    super();
  }
  ngOnInit() {
  }

}
