import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PiecesOneComponent } from './pieces-one.component';

describe('PiecesOneComponent', () => {
  let component: PiecesOneComponent;
  let fixture: ComponentFixture<PiecesOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PiecesOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PiecesOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
