import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { PieceBase, Piece, PIECE_NAME } from '../../../piece.model';

@Component({
  selector: 'xopro-pieces-one',
  templateUrl: './pieces-one.component.html',
  styleUrls: ['./pieces-one.component.scss']
})
export class PiecesOneComponent extends PieceBase implements OnInit, Piece {
   name = PIECE_NAME.DARKBOARD;
  constructor() {
    super();
  }

  ngOnInit() {
  }

}
