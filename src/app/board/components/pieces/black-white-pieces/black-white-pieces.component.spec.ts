import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackWhitePiecesComponent } from './black-white-pieces.component';

describe('BlackWhitePiecesComponent', () => {
  let component: BlackWhitePiecesComponent;
  let fixture: ComponentFixture<BlackWhitePiecesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackWhitePiecesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackWhitePiecesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
