import { Component, OnInit } from '@angular/core';
import { PieceBase, PIECE_NAME, Piece } from '../../../piece.model';

@Component({
  selector: 'xopro-black-white-pieces',
  templateUrl: './black-white-pieces.component.html',
  styleUrls: ['./black-white-pieces.component.scss']
})
export class BlackWhitePiecesComponent  extends PieceBase  implements OnInit, Piece {
  name = PIECE_NAME.BLACKWHITE;

  constructor() {
    super();
  }

  ngOnInit() {
  }

}
