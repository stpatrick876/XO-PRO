import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackSilverBoardComponent } from './black-silver-board.component';

describe('BlackSilverBoardComponent', () => {
  let component: BlackSilverBoardComponent;
  let fixture: ComponentFixture<BlackSilverBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackSilverBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackSilverBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
