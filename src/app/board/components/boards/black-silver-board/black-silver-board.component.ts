import { Component, OnInit } from '@angular/core';
import { IBoardComponent } from '../../../board.model';
import { Input } from '@angular/core';
import { ComponentFactoryResolver } from '@angular/core';
import { PieceService } from '../../../services/piece/piece.service';
import { BoardComponentBase } from '../../../BoardComponentBase';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../core/store';

@Component({
  selector: 'xopro-black-silver-board',
  templateUrl: './black-silver-board.component.html',
  styleUrls: ['./black-silver-board.component.scss']
})
export class BlackSilverBoardComponent extends BoardComponentBase implements OnInit, IBoardComponent  {
  @Input() data: any;

  constructor( protected pieceService: PieceService, protected componentFactoryResolver: ComponentFactoryResolver, protected store$: Store<AppState.State>) {
    super(pieceService, componentFactoryResolver, store$);
  }
  ngOnInit() {}

}
