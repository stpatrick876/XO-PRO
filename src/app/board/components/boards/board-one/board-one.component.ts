import { Component, OnInit } from '@angular/core';
import { IBoardComponent } from '../../../board.model';
import { Input } from '@angular/core';
import { ComponentFactoryResolver } from '@angular/core';
import { PieceService } from '../../../services/piece/piece.service';
import { BoardComponentBase } from '../../../BoardComponentBase';
import { AppState } from '../../../../core/store';
import { Store } from '@ngrx/store';
@Component({
  selector: 'xopro-board-one',
  templateUrl: './board-one.component.html',
  styleUrls: ['./board-one.component.scss']
})
export class BoardOneComponent extends BoardComponentBase implements OnInit, IBoardComponent {

  @Input() data: any;

  constructor( protected pieceService: PieceService, protected componentFactoryResolver: ComponentFactoryResolver, protected store$: Store<AppState.State>) {
    super(pieceService, componentFactoryResolver, store$);
  }

  ngOnInit() {}


}
