import { Component, OnInit } from '@angular/core';
import { IBoardComponent } from '../../../board.model';
import { ComponentFactoryResolver } from '@angular/core';
import { PieceService } from '../../../services/piece/piece.service';
import { BoardComponentBase } from '../../../BoardComponentBase';
import { Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../../core/store';

@Component({
  selector: 'xopro-black-white-board',
  templateUrl: './black-white-board.component.html',
  styleUrls: ['./black-white-board.component.scss']
})
export class BlackWhiteBoardComponent extends BoardComponentBase  implements OnInit, IBoardComponent {
  @Input() data: any;

  constructor( protected pieceService: PieceService, protected componentFactoryResolver: ComponentFactoryResolver, protected store$: Store<AppState.State>) {
    super(pieceService, componentFactoryResolver, store$);
  }

  ngOnInit() {
  }

}
