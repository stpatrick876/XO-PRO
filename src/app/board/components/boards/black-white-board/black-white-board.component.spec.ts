import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackWhiteBoardComponent } from './black-white-board.component';

describe('BlackWhiteBoardComponent', () => {
  let component: BlackWhiteBoardComponent;
  let fixture: ComponentFixture<BlackWhiteBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackWhiteBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackWhiteBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
