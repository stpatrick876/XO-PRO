import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { DynamicPieceItem, PIECE_NAME } from '../../piece.model';
import { PiecesOneComponent } from '../../components/pieces/pieces-one/pieces-one.component';
import { BlackWhitePiecesComponent } from '../../components/pieces/black-white-pieces/black-white-pieces.component';
import { BlackSilverPiecesComponent } from '../../components/pieces/black-silver-pieces/black-silver-pieces.component';

@Injectable()
export class PieceService {
  constructor() { }

  getPieces() {
    return of( [
      new DynamicPieceItem(PiecesOneComponent, {name: PIECE_NAME.DARKBOARD}),
      new DynamicPieceItem(BlackWhitePiecesComponent, {name: PIECE_NAME.BLACKWHITE}),
      new DynamicPieceItem(BlackSilverPiecesComponent, {name: PIECE_NAME.BLACKSILVER})
    ]);
  }

  getPiece(name: PIECE_NAME) {
    return this.getPieces().map(pieces =>  pieces.filter(piece =>  piece.data.name == name)[0]);
  }
}
