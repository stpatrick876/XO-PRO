import { Injectable } from '@angular/core';
import { BOARD_NAME, DynomicBoardItem } from '../../board.model';
import { of } from 'rxjs';
import { BoardOneComponent } from '../../components/boards/board-one/board-one.component';
import { BlackWhiteBoardComponent } from '../../components/boards/black-white-board/black-white-board.component';
import { BlackSilverBoardComponent } from '../../components/boards/black-silver-board/black-silver-board.component';

@Injectable()
export class BoardService {
  constructor() { }

  getBoards() {
    return of([
      new DynomicBoardItem(BoardOneComponent, {name: BOARD_NAME.DARKBOARD}),
      new DynomicBoardItem(BlackWhiteBoardComponent, {name: BOARD_NAME.BLACKWHITE}),
      new DynomicBoardItem(BlackSilverBoardComponent, {name: BOARD_NAME.BLACKSILVER})
    ]);
  }

  getBoard(name: BOARD_NAME) {
    return this.getBoards().map(boards =>  boards.filter(board =>  board.data.name == name)[0]);
  }


}
