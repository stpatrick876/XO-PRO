import { PieceHostDirective } from "./directives/piece-host/piece-host.directive";
import { PieceType, DynamicPieceItem, IDynamicPieceItem } from "./piece.model";
import { ViewChildren } from "@angular/core";
import { Input } from "@angular/core";
import { QueryList } from "@angular/core";
import { PieceService } from "./services/piece/piece.service";
import { ComponentFactoryResolver } from "@angular/core";
import { ComponentRef } from "@angular/core";
import { Observable, of } from "rxjs";
import { AppState } from "../core/store";
import { Store } from "@ngrx/store";
import { GamePlayActions, GamePlaySelectors as fromGame } from "../algorithm/store";
import { CommonService } from "../shared/services/common/common.service";
import { User } from "../auth/model";

export class BoardComponentBase {
  pieceType = PieceType;
  @Input() type: PieceType;
  @ViewChildren(PieceHostDirective) pieceHostList: QueryList<PieceHostDirective>;
  cells = Array(9).fill(1).map((x,i)=>i);
  board: any;
  constructor(protected pieceService: PieceService,
    protected componentFactoryResolver: ComponentFactoryResolver,
  protected store$: Store<AppState.State>){
    store$.select(fromGame.selectBoard).subscribe(board => {
      this.board = board;
    })
  }


  isDropAllowed(i: number) {


    const dropHandler = (e) => {
      const cell = CommonService._.find(this.board, { 'id': i});

      return cell.piece == PieceType.EMPTY;

    }

    return dropHandler;
  };



  onPieceDrop(e: any, cellIndex:  number) {
    this.pieceService.getPiece(e.dragData.name).subscribe((piece: DynamicPieceItem) => {
      this.store$.dispatch(new GamePlayActions.MakePlay({player: e.dragData.player, type: e.dragData.type, cellIndex: (cellIndex + 1)}))
      this.loadPieceComponentInCell(cellIndex, piece, e.dragData.type, e.dragData.player).subscribe(() => {
        this.store$.dispatch(new GamePlayActions.CheckTerminal())

      })
    });
  }

  loadPieceComponentInCell(cellIndex: number, piece: DynamicPieceItem, type: PieceType, player?: User): Observable<any> {
    console.log('cell index ', cellIndex)
    const cellHost  = this.pieceHostList.toArray()[cellIndex];

           if(!piece) {
             return of(false);
           }
           let componentFactory = this.componentFactoryResolver.resolveComponentFactory(piece.component);
           let viewContainerRef = cellHost.viewContainerRef;
           viewContainerRef.clear();

           let componentRef = viewContainerRef.createComponent(componentFactory);


           const data = piece.data;
           data.type = PieceType[PieceType[type]];
         (<IDynamicPieceItem>componentRef.instance).data = data;
         cellHost.setColor(player.color);
         return of(componentRef);
  }

  makePlay() {

    this.store$.dispatch(new GamePlayActions.MakePlay())

  }
}
