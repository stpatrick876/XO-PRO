import { Directive } from '@angular/core';
import { ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[xoproBoardHost]'
})
export class BoardHostDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
