import { Directive } from '@angular/core';
import { ViewContainerRef } from '@angular/core';
import { Renderer2 } from '@angular/core';

@Directive({
  selector: '[xoproPieceHost]'
})
export class PieceHostDirective {

  constructor(public viewContainerRef: ViewContainerRef, private renderer: Renderer2) { }

setColor(color){
  const nativeElement = this.viewContainerRef.element.nativeElement.parentElement;
  this.renderer.setStyle(nativeElement, 'background-color', color)
  this.renderer.setStyle(nativeElement, 'opacity', 0.4)


}
}
