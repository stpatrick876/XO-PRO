import { Input } from "@angular/core";
import { Type } from "@angular/core";

export enum PieceType {
 NOUGHT,
 CROSS,
 EMPTY
}

export enum PIECE_NAME {
  DARKBOARD = 'Dark Board Piece',
  BLACKWHITE = 'Black And White Piece',
  BLACKSILVER = 'Black And Silver Piece'
}

export interface Piece {
 type: PieceType;
 name: PIECE_NAME;
}


export class PieceBase {
  pieceType = PieceType;
  @Input() type: PieceType;
}

export interface IDynamicPieceItem  {
  data: any;
}
export class DynamicPieceItem {
  constructor(public component: Type<any>, public data: any) {}
}
