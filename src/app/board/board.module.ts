import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardHostDirective } from './directives/board-host/board-host.directive';
import { BoardService } from './services/board/board.service';
import { BoardOneComponent } from './components/boards/board-one/board-one.component';
import { BlackWhiteBoardComponent } from './components/boards/black-white-board/black-white-board.component';
import { BlackSilverBoardComponent } from './components/boards/black-silver-board/black-silver-board.component';
import { BlackSilverPiecesComponent } from './components/pieces/black-silver-pieces/black-silver-pieces.component';
import { PiecesOneComponent } from './components/pieces/pieces-one/pieces-one.component';
import { BlackWhitePiecesComponent } from './components/pieces/black-white-pieces/black-white-pieces.component';
import { PieceHostDirective } from './directives/piece-host/piece-host.directive';
import { SharedModule } from '../shared/shared.module';
import { NgDragDropModule } from 'ng-drag-drop';
import { PieceService } from './services/piece/piece.service';
const pieces = [BlackWhitePiecesComponent, PiecesOneComponent, BlackSilverPiecesComponent];
const boards = [ BoardOneComponent, BlackWhiteBoardComponent, BlackSilverBoardComponent];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NgDragDropModule.forRoot()


  ],
  providers: [BoardService, PieceService],
  declarations: [BoardHostDirective, PieceHostDirective, ...boards, ...pieces],
  exports: [BoardHostDirective, PieceHostDirective, ...boards, ...pieces],
    entryComponents: [ ...boards, ...pieces]
})
export class BoardModule { }
