import { Type } from '@angular/core';
import { Input } from '@angular/core';
import { PieceType, IDynamicPieceItem } from './piece.model';



export interface IBoardComponent {
  data: any;
  onPieceDrop(e: any, cellIndex:  number);
}

export class DynomicBoardItem {
  constructor(public component: Type<any>, public data: any) {}
}

export enum BOARD_NAME {
  DARKBOARD = 'Dark Board',
  BLACKWHITE = 'Black And White',
  BLACKSILVER = 'Black And Silver'
}

export interface BoardOption {
name: BOARD_NAME,
image: string
}
export const boardOptions: BoardOption[] = [
  {
    name: BOARD_NAME.DARKBOARD,
    image: ''
  },
  {
    name: BOARD_NAME.BLACKWHITE,
    image: ''
  },
  {
    name: BOARD_NAME.BLACKSILVER,
    image: ''
  }
]
