import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { RootStoreState } from '../../core/store';
import { AuthState } from '../../auth/store';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private store$: Store<AuthState.State>) {}

   canActivate(): Observable<boolean> {
     return this.store$.select('auth')
                .take(1)
                .switchMap(auth => {
                  if (!auth) {
                      return of(false);
                  }
                  return of(true);

                })
   }
}
