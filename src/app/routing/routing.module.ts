import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GameMenuComponent } from '../core/components/game-menu/game-menu.component';
import { HomeComponent } from '../core/components/home/home.component';
import { LoginComponent } from '../auth/login/login.component';
import { AuthGuard } from './auth-guard/auth.gaurd';
import { NotFoundComponent } from '../core/components/not-found/not-found.component';
import { GameComponent } from '../game/components/game/game.component';
import { AppSettingsComponent } from '../settings/components/app-settings/app-settings.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthGuard] ,
    children: [
      {
        path: '',
        redirectTo: 'menu',
        pathMatch: 'full',
      },
      {
        path: 'menu',
        component: GameMenuComponent,
      },
      {
        path: 'settings',
        component: AppSettingsComponent,
      },
      {
        path: 'game',
        component: GameComponent
      }

    ]
  },
  { component: NotFoundComponent, path: "**", data: {pageTitle : 'Page Not Found'} }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes)
  ],
  exports: [ RouterModule ],
  providers: [AuthGuard ]
})
export class AppRoutingModule { }
