import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { AppComponent } from './core/components/main/app.component';
import { HomeComponent } from './core/components/home/home.component';
import { GameMenuComponent } from './core/components/game-menu/game-menu.component';
import { StoreModule } from '@ngrx/store';
import { SettingsModule } from './settings/settings.module';
import { HeaderComponent } from './core/components/header/header.component';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './routing/routing.module';
import { AuthModule } from './auth/auth.module';
import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { reducers, metaReducers } from './core/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NotFoundComponent } from './core/components/not-found/not-found.component';
import { GameModule } from './game/game.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BoardModule } from './board/board.module';
import { AlgorithmModule } from './algorithm/algorithm.module';
import { AnimationsService } from './animation.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GameMenuComponent,
    HeaderComponent,
    NotFoundComponent

    ],
  imports: [
    BrowserModule,
    SharedModule,
    SettingsModule,
    AuthModule,
    AppRoutingModule,
    GameModule,
    AlgorithmModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({
      name: 'NgRx Book Store DevTools',
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([]),
    AngularFireModule.initializeApp(environment.firebase),
    BrowserAnimationsModule,
  ],
  providers: [AnimationsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
