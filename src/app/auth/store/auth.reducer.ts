import * as userActions from './auth.actions';
import { User } from '../model';
import { State, initialState } from './state';

export type Action = userActions.All;

// const defaultUser = new User(null, 'GUEST');

/// Reducer function
export function userReducer(state: State = initialState, action: Action) {
  switch (action.type) {

    case userActions.GET_USER:
        return { ...state, isLoading: true };

    case userActions.AUTHENTICATED:
        return { ...state, ...action.payload, isLoading: false };

    case userActions.NOT_AUTHENTICATED:
        return { ...state, ...action.payload, isLoading: false };

    case userActions.GOOGLE_LOGIN:
      return { ...state, isLoading: true };

      case userActions.GUEST_LOGIN:
      return { ...state, isLoading: true };

    case userActions.AUTH_ERROR:
      return { ...state, ...action.payload, isLoading: false };

    case userActions.LOGOUT:
      return { ...state, isLoading: true };
      default:
          return state;
  }
}
