import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of as observableOf, of, forkJoin } from 'rxjs';
import 'rxjs/add/operator/merge';

import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/delay';

import { User, UserType } from '../model';
import * as firebase from 'firebase';

import * as userActions from './auth.actions';
import { AngularFireAuth } from 'angularfire2/auth';
import { LocalStorageService } from '../../shared/services/localstorage/localstorage.service';
import { UniqueGeneratorService } from '../../shared/services/uniqueGenerator/unique-genrator.service';
import { Router } from '@angular/router';
export type Action = userActions.All;

@Injectable()
export class AuthEffects {
  constructor(private actions$: Actions, private afAuth: AngularFireAuth, private _localStorage: LocalStorageService,
    private unique: UniqueGeneratorService, private router: Router) {}


    @Effect()
       getUser:  Observable<Action> = this.actions$.ofType(userActions.GET_USER)

           .map((action: userActions.GetUser) => action.payload )
           .switchMap(payload => this.afAuth.authState )
           .delay(2000) // delay to show loading spinner, delete me!
           .map( googleUserData => {
               if (googleUserData) {
                   /// User logged in
                   const user = new User(googleUserData.uid, googleUserData.displayName, UserType.GOOGLE, googleUserData.photoURL, this.unique.getUserColor);

                   return new userActions.Authenticated({user: user});
               } else {
                 var guestUser = this._localStorage.select('user');
                 if(guestUser) {
                   const user = new User(guestUser.uid, guestUser.displayName, UserType.GUEST, guestUser.avatar, this.unique.getUserColor);
                   return new userActions.Authenticated({user: user});
                 } else {
                   /// User not logged in
                   return new userActions.NotAuthenticated({user: null});
                 }
               }
           })
           .catch(err =>  Observable.of(new userActions.AuthError()) );


   @Effect()
   login:  Observable<Action> = this.actions$.ofType(userActions.GOOGLE_LOGIN)

       .map((action: userActions.GoogleLogin) => action.payload)
       .switchMap(payload => {
           return Observable.fromPromise( this.googleLogin() );
       })
       .map( credential => {
           // successful login
           this.router.navigate(['/home'])
           return new userActions.GetUser();
       })
       .catch(err => {
           return Observable.of(new userActions.AuthError({error: err.message}));
       });



       @Effect()
   guestLogin:  Observable<Action> = this.actions$.ofType(userActions.GUEST_LOGIN)

       .map((action: userActions.GuestLogin) => action.payload)
       .switchMap(payload => {
         const guestUser = {
          uid: this.unique.guid,
          avatar: `http://api.adorable.io/avatars/285/${this.unique.randomString}.png`,
          displayName: this.unique.guestName
         };


        return of(this._localStorage.set('user', guestUser));
    }).map( res => {
      // successful login
      this.router.navigate(['/home'])

      return new userActions.GetUser();
  });





       @Effect()
      logout:  Observable<Action> = this.actions$.ofType(userActions.LOGOUT)

    .map((action: userActions.Logout) => action.payload )
    .switchMap(payload => {
      this._localStorage.remove('user');
        return of( this.afAuth.auth.signOut() );
    })
    .map( authData => {
      this.router.navigate(['/login'])
        return new userActions.GetUser();
    })
    .catch(err => of(new userActions.AuthError({error: err.message})) );



   private googleLogin():  Promise<any> {
       const provider = new firebase.auth.GoogleAuthProvider();
       return this.afAuth.auth.signInWithPopup(provider);
   }




}
