import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoaderService } from 'src/app/shared/services/loader/loader.service';
import { userActions, AuthState } from '../store';
import { RootStoreState } from '../../core/store';
import { CommonService } from '../../shared/services/common/common.service';

@Component({
  selector: 'xopro-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor( private loaderService: LoaderService, private store$: Store<AuthState.State>  ) { }

  ngOnInit() {
  }

  googleLogin() {
    this.store$.dispatch(new userActions.GoogleLogin());
  }

  guestLogin() {
    this.store$.dispatch(new userActions.GuestLogin());
  }

}
