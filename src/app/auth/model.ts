
export enum UserType {
  GOOGLE,
  GUEST,
  MACHINE = 'AI'
}

export interface IUser {
  uid:         string;
  avatar?: string;
  displayName: string;
  loading?:    boolean;
  color?:      any;
  error?:      string;
  type:        UserType;
}

export class User implements IUser {
  constructor(public uid: string, public displayName: string, public type: UserType, public avatar: string, public color:  any = '#d61ff5') {}
}


export const Machine: User = new User("1234567890", 'machine', UserType.MACHINE, "../../assets/images/futurproject.png")
