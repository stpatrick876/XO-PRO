import { Action } from "@ngrx/store";

export const START_GAME              = '[GAME PLAY] Start Game';
export const GAME_STARTED            = '[GAME PLAY] Game Started';
export const MAKE_PLAY               = '[[GAME PLAY] Make Play';
export const ADVANCE_TURN            = '[[GAME PLAY] Advance Turn';
export const UPDATE_SHADOWBOARD      = '[[GAME PLAY] Update ShadowBoard';
export const CHECK_TERMINAL          = '[[GAME PLAY] Check Terminal';
export const IS_TERMINAL     = '[[GAME PLAY] Is Terminal';


export class AdvanceTurn implements Action {
    readonly type = ADVANCE_TURN;
    constructor(public payload?: any) {}
}

export class StartGame implements Action {
    readonly type = START_GAME;
    constructor() {}
}

export class GameStarted implements Action {
    readonly type = GAME_STARTED;
    constructor() {}
}
export class MakePlay implements Action {
    readonly type = MAKE_PLAY;
    constructor(public payload?: any) {}
}

export class UpdateShadowboard implements Action {
    readonly type = UPDATE_SHADOWBOARD;
    constructor(public payload?: any) {}
}

export class CheckTerminal implements Action {
    readonly type = CHECK_TERMINAL;
    constructor(public payload?: any) {}
}
export class IsTerminal implements Action {
    readonly type = IS_TERMINAL;
    constructor(public payload?: any) {}
}

export type All = AdvanceTurn | StartGame | GameStarted | MakePlay | UpdateShadowboard | CheckTerminal | IsTerminal;
