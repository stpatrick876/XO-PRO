import { GAME_STATUS, ShadowBoardCell } from "../game-play.model";
import { PieceType } from "../../board/piece.model";
import { PLAYER_TYPE } from "../../game/game.model";


export interface State {
  turn: PLAYER_TYPE | null; //the player who has the turn to player
  oMovesCount?: number; // the player who has the turn to player
  result: string; // the result of the game in this State
  board: ShadowBoardCell[]; // the board configuration in this state
  status: GAME_STATUS,
  error: any;
}


export const initialState: State = {
  turn: null,
  oMovesCount: 0,
  status: GAME_STATUS.BEGINNING,
  result: "still running",
  board:  [
    {
      id: 1,
      piece: PieceType.EMPTY,
      player: null
    },
    {
      id: 2,
      piece: PieceType.EMPTY,
      player: null
    },
    {
      id: 3,
      piece: PieceType.EMPTY,
      player: null
    },
    {
      id: 4,
      piece: PieceType.EMPTY,
      player: null
    },
    {
      id: 5,
      piece: PieceType.EMPTY,
      player: null
    },
     {
        id:6,
        piece: PieceType.EMPTY,
        player: null
    },
     {
        id: 7,
        piece: PieceType.EMPTY,
        player: null
    },
    {
        id: 8,
        piece: PieceType.EMPTY,
        player: null
      },
      {
          id: 9,
          piece: PieceType.EMPTY,
          player: null
        }

  ],
  error: null
}
