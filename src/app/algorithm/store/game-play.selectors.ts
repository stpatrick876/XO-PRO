import {
  createFeatureSelector,
  createSelector,
  MemoizedSelector
} from '@ngrx/store';

import { State } from './state';
import { GAME_STATUS } from '../game-play.model';


const getError = (state: State): any => state.error;

const getBoard = (state: State): any => state.board;

const getTurn = (state: State): any => state.turn;

const getGameStatus = (state: State): any => state.status;


export const selectGameState: MemoizedSelector<object, State> = createFeatureSelector<State>('gamePlay');

export const selectBoard: MemoizedSelector<object, any[]> = createSelector(selectGameState, getBoard);

export const selectTurn: MemoizedSelector<object, any> = createSelector(selectGameState, getTurn);

export const selectGameStatus: MemoizedSelector<object, GAME_STATUS> = createSelector(selectGameState, getGameStatus);
