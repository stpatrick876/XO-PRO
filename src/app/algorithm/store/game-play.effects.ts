import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { Observable, of as observableOf, of, forkJoin } from "rxjs";
import "rxjs/add/operator/withLatestFrom";

import * as gamePlayActions from "./game-play.actions";
import { Router } from "@angular/router";
import { PieceType } from "../../board/piece.model";
import { CommonService } from "../../shared/services/common/common.service";
import { PLAYER_TYPE } from "../../game/game.model";
import { Store } from "@ngrx/store";
import { GamePlayState } from ".";
import { ShadowBoardCell, GameState } from "../game-play.model";
import { GamePlayService } from "../services/game-play/game-play.service";
export type Action = gamePlayActions.All;
import { Board } from "../Board.model";

@Injectable()
export class GamePlayEffects {
  constructor(
    private actions$: Actions,
    private store$: Store<GamePlayState.State>,
    private gamePlaySrv: GamePlayService
  ) {}

  @Effect()
  startGame: Observable<Action> = this.actions$
    .ofType(gamePlayActions.START_GAME)
    .map((action: gamePlayActions.StartGame) => {})
    .switchMap(() => [
      new gamePlayActions.AdvanceTurn(
        CommonService._.sample([PLAYER_TYPE.OPONENT, PLAYER_TYPE.PLAYER])
      ),
      new gamePlayActions.GameStarted()
    ]);

  @Effect()
  makePlay: Observable<Action> = this.actions$
    .ofType(gamePlayActions.MAKE_PLAY)
    .map((action: gamePlayActions.MakePlay) => action.payload)
    .switchMap(payload => {
      const data = {
        id: payload.cellIndex,
        player: payload.player,
        piece: PieceType[payload.type]
      };
      return of(new gamePlayActions.UpdateShadowboard(data));
    });

  @Effect()
  checkTerminal: Observable<Action> = this.actions$
    .ofType(gamePlayActions.CHECK_TERMINAL)
    .map((action: gamePlayActions.CheckTerminal) => action.payload)
    .withLatestFrom(this.store$)
    .switchMap(res => {
      const payload = res[0];
      const store: any = res[1];
      const board: ShadowBoardCell[] = store.gamePlay.board;

      const gameState = new Board(board.map(GamePlayService.mapAppBoardState)).isTerminal();

      if (gameState ===  false) {
        return of(new gamePlayActions.AdvanceTurn());
      } else {
        return of(new gamePlayActions.IsTerminal(gameState.winner));
      }
    });
}
