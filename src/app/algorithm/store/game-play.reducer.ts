import * as gamePlayActions from './game-play.actions';
import { State, initialState } from "./state";
import { GAME_STATUS } from '../game-play.model';
import { GamePlayService } from '../services/game-play/game-play.service';
import { PLAYER_TYPE } from '../../game/game.model';

export type Action = gamePlayActions.All;
/// Reducer function
export function gamePlayReducer(state: State = initialState, action: Action) {
  switch (action.type) {
    case gamePlayActions.START_GAME:
        return { ...state };
    case gamePlayActions.GAME_STARTED:
        return { ...state, status: GAME_STATUS.RUNNING };
    case gamePlayActions.UPDATE_SHADOWBOARD:
         const updatedShadowBoard = GamePlayService.updateShadowboard(state.board, action.payload);
        return { ...state, board: updatedShadowBoard };
    case gamePlayActions.ADVANCE_TURN:
        let turn: PLAYER_TYPE;
        if(action && action.payload) {
          turn = action.payload
        } else {
          turn = state.turn === PLAYER_TYPE.PLAYER ? PLAYER_TYPE.OPONENT : PLAYER_TYPE.PLAYER;
        }

        return { ...state, turn: turn };
        case gamePlayActions.IS_TERMINAL:
        return { ...state,  status: GAME_STATUS.ENDED, winner: action.payload.winner};
        default:
            return state;

  }
}
