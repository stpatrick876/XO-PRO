import { PieceType } from "../board/piece.model";
import { User } from "../auth/model";

export enum GAME_STATUS {
  BEGINNING   = 'beginning',
  RUNNING = 'running',
  ENDED = 'ended'
}


export interface ShadowBoardCell {
    id: number;
    piece : PieceType;
    player: User | null;
}


export interface GameState {
  isTerminal: Boolean;
  winner: User | null;
}
