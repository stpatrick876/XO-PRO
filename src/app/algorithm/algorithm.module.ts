import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamePlayService } from './services/game-play/game-play.service';
import { AiService } from './services/ai/ai.service';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { GamePlayEffects } from './store/game-play.effects';
import { gamePlayReducer } from './store/game-play.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('gamePlay', gamePlayReducer),
    EffectsModule.forFeature([GamePlayEffects]),
  ],

  declarations: [],
  providers: [GamePlayEffects, GamePlayService, AiService]
})
export class AlgorithmModule { }
