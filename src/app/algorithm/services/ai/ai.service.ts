import { Injectable } from "@angular/core";
import { ShadowBoardCell, GameState } from "../../game-play.model";
import { Store } from "@ngrx/store";
import {
  GamePlayState,
  GamePlaySelectors as fromGamePlay,
  GamePlayActions
} from "../../store";
import { PieceType } from "../../../board/piece.model";
import { of } from "rxjs";
import { PLAYER_TYPE, DificultyLevel } from "../../../game/game.model";
import { Map } from "immutable";
import { GamePlayService } from "../game-play/game-play.service";
import { UserType, User } from "../../../auth/model";
import { CommonService } from "../../../shared/services/common/common.service";
import { GameSetupSelectors } from "../../../game/store/game-setup";
import { Board } from "../../Board.model";
import { AiAction } from "../../AiAction";

@Injectable()
export class AiService {
  board: any;
  players: any;

  constructor(
    private store$: Store<GamePlayState.State>,
    private gamePlaySrv: GamePlayService
  ) {
    store$.select(fromGamePlay.selectBoard).subscribe(board => {
      this.board = board;
    });
    store$.select(GameSetupSelectors.selectGamePlayers).subscribe(players => {
      this.players = players;
    });
  }

  getEmptyCells = (board: ShadowBoardCell[] = this.board) =>
    board.filter(
      cell => cell.piece === PieceType.EMPTY && cell.player === null
    );

  takeAMasterMove(): any {
    throw new Error("Method not implemented.");
  }

  takeANoviceMove(player, piece): any {
    const boardState = this.board.slice().map(GamePlayService.mapAppBoardState);
    let board = new Board(boardState);
    let p = new AiAction();
    const aiMove = p.getBestMove(board, true);
    const cellIndex = Number(aiMove) + 1;
    const cell = this.board.filter(cell => cell.id === cellIndex)[0];

    this.store$.dispatch(
      new GamePlayActions.MakePlay({
        player: player,
        type: piece.label,
        cellIndex: cellIndex
      })
    );

    return cell;
  }

  takeABlindMove(player, piece): any {
    const emptyCells = this.getEmptyCells();
    var randomCell = emptyCells[Math.floor(Math.random() * emptyCells.length)];

    this.store$.dispatch(
      new GamePlayActions.MakePlay({
        player: player,
        type: piece.label,
        cellIndex: randomCell.id
      })
    );

    return randomCell;
  }

  /*
     * notify the ai player that it's its turn
     * @param turn [String]: the player to play, either X or O
     */
  notify(levelOfIntelligence, player, piece) {
    let cell: ShadowBoardCell;

    switch (levelOfIntelligence) {
      //invoke the desired behavior based on the level chosen
      case DificultyLevel.EASY:
        cell = this.takeABlindMove(player, piece);
        break;
      case DificultyLevel.NOVICE:
        cell = this.takeANoviceMove(player, piece);
        break;
      case DificultyLevel.MASTER:
        cell = this.takeAMasterMove();
        break;
    }

    return of(cell);
  }
}
