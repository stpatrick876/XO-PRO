import { Injectable } from "@angular/core";
import { ShadowBoardCell, GameState } from "../../game-play.model";
import { CommonService } from "../../../shared/services/common/common.service";
import { of } from "rxjs";
import { PieceType } from "../../../board/piece.model";
import { Observable } from "rxjs/internal/Observable";

@Injectable()
export class GamePlayService {
  constructor() {}

  static updateShadowboard(board: ShadowBoardCell[], cellData: any) {
    const boardsCopy = CommonService._.map(board, CommonService._.clone);
    const cell = CommonService._.find(boardsCopy, { id: cellData.id });
    cell.player = cellData.player;
    cell.piece = cellData.piece;

    return boardsCopy;
  }

  static mapAppBoardState(cell: ShadowBoardCell) {
    if (cell.piece === 1 || cell.piece.toString() === PieceType[1]) return "x";
    if (cell.piece === 0 || cell.piece.toString() === PieceType[0]) return "o";
    if (cell.piece === 2 || cell.piece.toString() === PieceType[2]) return "";
  }
}
